(function ($) {

  $('.js-sub-pop').magnificPopup({
    type: 'inline',
  });


  /**
   * Newsletter Popup
   */

  // var is_popup_closed = Cookies.get('is_popup_closed');

  // if ( is_popup_closed != 'true' ) {
  //   setTimeout( open_newsletter_popup, 5000 );
  // }

  // $(document).mouseleave(function () {
  //   var is_popup_closed = Cookies.get('is_popup_closed');

  //   if ( is_popup_closed != 'true' ) {
  //     open_newsletter_popup();
  //   }
  // });

  // function open_newsletter_popup() {
  //   var $newsletter_popup = $('#js-newsletter-popup');

  //   if ( $newsletter_popup[0] ) {
  //     $.magnificPopup.open({
  //       items: {
  //         src: $newsletter_popup[0],
  //       },
  //       type: 'inline',
  //       callbacks: {
  //         close: function () {
  //           Cookies.set( 'is_popup_closed', 'true', { expires: 30 });
  //         },
  //       }
  //     });
  //   }
  // }


  /**
   * Mega Menu
   */

  var $animation_trigger_text = $('.slide-up-fade-in');

  $animation_trigger_text.waypoint({
    handler: function ( direction ) {
      if ( direction === 'down' ) {

        var $this = $(this.element);

        if ( ! $this.hasClass( 'slide-up-fade-in-is-animate' ) ) {
          $this.addClass( 'slide-up-fade-in-is-animate' );
        }
      }
    },
    offset: '80%'
  });


  var $animation_trigger_image = $('.slide-down-fade-in');

  $animation_trigger_image.waypoint({
    handler: function ( direction ) {
      if ( direction === 'down' ) {

        var $this = $(this.element);

        if ( ! $this.hasClass( 'slide-down-fade-in-is-animate' ) ) {
          $this.addClass( 'slide-down-fade-in-is-animate' );
        }
      }
    },
    offset: '80%'
  });


  var mouseOutTimeout = 500;
  var mouse_delay = 350;

  var unset_active_delay = 0;
  var mouseleave_timer;
  var delay_timer;
  var first_leave_timer;
  var is_first_leave = false;

  var $max_screen_width = 796;
  var $screen_width = $(window).width();

  $(window).resize(function () {
    $screen_width = $(window).width();
  });

  function show_mega_dropdown( $dropdown_el ) {
    if ( $screen_width > $max_screen_width ) {
      $dropdown_el.addClass('slide-down').removeClass('fadeout');
    }
  }

  function hide_mega_dropdown( $dropdown_el ) {
    if ( $screen_width > $max_screen_width ) {
      $dropdown_el.removeClass('slide-down').addClass('fadeout');
    }
  }

  // $('.navigation--main > ul > li > a:not(.tech-dropdown):not(.app-dropdown):not(.about-dropdown)a:not(.product-dropdown)').on('mouseenter', function () {
  //   hide_mega_dropdown( $('#js-mmi--tech, #js-mmi--app, #js-mmi--about, #js-mmi--product') );
  // });

  $('.energy-storage-dropdown').on('mouseenter', function () {
    hide_mega_dropdown( $('#js-mmi--energy-storage, #js-mmi--app, #js-mmi--about, #js-mmi--tech') );
    show_mega_dropdown( $('#js-mmi--energy-storage') );
  });
  $('.tech-dropdown').on('mouseenter', function () {
    hide_mega_dropdown( $('#js-mmi--energy-storage, #js-mmi--app, #js-mmi--about') );
    show_mega_dropdown( $('#js-mmi--tech') );
  });
  $('.app-dropdown').on('mouseenter', function () {
    hide_mega_dropdown( $('#js-mmi--energy-storage, #js-mmi--tech, #js-mmi--about') );
    show_mega_dropdown( $('#js-mmi--app') );
  });
  $('.about-dropdown').on('mouseenter', function () {
    hide_mega_dropdown( $('#js-mmi--energy-storage, #js-mmi--tech, #js-mmi--app') );
    show_mega_dropdown( $('#js-mmi--about') );
  });
  $('.product-dropdown').on('mouseenter', function () {
    hide_mega_dropdown( $('#js-mmi--energy-storage, #js-mmi--tech, #js-mmi--app', '#js-mmi--about') );
    show_mega_dropdown( $('#js-mmi--product') );
  });

  $('.energy-storage-dropdown, #js-mmi--energy-storage').hover(
    function () {
      mouse_delay = 350;

      clearTimeout( mouseleave_timer );

      show_mega_dropdown( $('#js-mmi--energy-storage') );

      if ( is_first_leave === true ) {
        mouse_delay = 0;
      }

      delay_timer = setTimeout(function () {
        unset_active_delay = mouseOutTimeout;
      }, mouse_delay );

    },
    function () {
      mouseleave_timer = setTimeout(function () {
        hide_mega_dropdown( $('#js-mmi--energy-storage') );
      }, unset_active_delay );

      unset_active_delay = 0;
      clearTimeout( delay_timer );

      is_first_leave = true;

      clearTimeout( first_leave_timer );
      first_leave_timer = setTimeout(function () {
        is_first_leave = false;
      }, 350 );
    }
  );

  $('.tech-dropdown,#js-mmi--tech').hover(
    function () {
      mouse_delay = 350;

      clearTimeout( mouseleave_timer );

      show_mega_dropdown( $('#js-mmi--tech') );

      if ( is_first_leave === true ) {
        mouse_delay = 0;
      }

      delay_timer = setTimeout(function () {
        unset_active_delay = mouseOutTimeout;
      }, mouse_delay );

    },
    function () {
      mouseleave_timer = setTimeout(function () {
        hide_mega_dropdown( $('#js-mmi--tech') );
      }, unset_active_delay );

      unset_active_delay = 0;
      clearTimeout( delay_timer );

      is_first_leave = true;

      clearTimeout( first_leave_timer );
      first_leave_timer = setTimeout(function () {
        is_first_leave = false;
      }, 350 );
    }
  );
  $('.app-dropdown,#js-mmi--app').hover(
    function () {
      mouse_delay = 350;

      clearTimeout( mouseleave_timer );

      show_mega_dropdown( $('#js-mmi--app') );

      if ( is_first_leave === true ) {
        mouse_delay = 0;
      }

      delay_timer = setTimeout(function () {
        unset_active_delay = mouseOutTimeout;
      }, mouse_delay );

    },
    function () {
      mouseleave_timer = setTimeout(function () {
        hide_mega_dropdown( $('#js-mmi--app') );
      }, unset_active_delay );

      unset_active_delay = 0;
      clearTimeout( delay_timer );

      is_first_leave = true;

      clearTimeout( first_leave_timer );
      first_leave_timer = setTimeout(function () {
        is_first_leave = false;
      }, 350 );
    }
  );
  $('.about-dropdown,#js-mmi--about').hover(
    function () {
      mouse_delay = 350;

      clearTimeout( mouseleave_timer );

      show_mega_dropdown( $('#js-mmi--about') );

      if ( is_first_leave === true ) {
        mouse_delay = 0;
      }

      delay_timer = setTimeout(function () {
        unset_active_delay = mouseOutTimeout;
      }, mouse_delay );
    },
    function () {
      mouseleave_timer = setTimeout(function () {
        hide_mega_dropdown( $('#js-mmi--about') );
      }, unset_active_delay );

      unset_active_delay = 0;
      clearTimeout( delay_timer );

      is_first_leave = true;

      clearTimeout( first_leave_timer );
      first_leave_timer = setTimeout(function () {
        is_first_leave = false;
      }, 350 );
    }
  );
  $('.product-dropdown, #js-mmi--product').hover(
    function () {
      mouse_delay = 350;

      clearTimeout( mouseleave_timer );

      show_mega_dropdown( $('#js-mmi--product') );

      if ( is_first_leave === true ) {
        mouse_delay = 0;
      }

      delay_timer = setTimeout(function () {
        unset_active_delay = mouseOutTimeout;
      }, mouse_delay );
    },
    function () {
      mouseleave_timer = setTimeout(function () {
        hide_mega_dropdown( $('#js-mmi--product') );
      }, unset_active_delay );

      unset_active_delay = 0;
      clearTimeout( delay_timer );

      is_first_leave = true;

      clearTimeout( first_leave_timer );
      first_leave_timer = setTimeout(function () {
        is_first_leave = false;
      }, 350 );
    }
  );

})(jQuery);
