
[x] Header - Fix background image size
[x] Header - Reduce height

[x] Sidebar - Add blog description above sub box
[x] Sidebar - Sub box - change title to "Subscribe to the Blog"

[x] Archive Template - Reduce Pagination size

[x] Archive Template - Display 8 posts per page
