// Utils
var gulp    = require('gulp');
var watch   = require('gulp-watch');
var gutil   = require('gulp-util');
var cat     = require('gulp-cat');
var concat  = require('gulp-concat');
var plumber = require('gulp-plumber');
var inject  = require('gulp-inject');
var rename  = require('gulp-rename');
var clone   = require('gulp-clone');
var through2  = require('through2');

// JS
var uglify  = require('gulp-uglify');

// SASS
var sass      = require('gulp-sass')(require('sass'));
var postcss   = require('gulp-postcss');
var sassGlob  = require('gulp-sass-glob');

// CSS
var csswring     = require('csswring');
var autoprefixer = require('autoprefixer');
var mqpacker     = require('css-mqpacker');

// SVG
var svgstore = require('gulp-svgstore');
var svgmin   = require('gulp-svgmin');

// Packaging Theme
var zip = require('gulp-zip');

// Paths
var paths = {};


/**
 * SASS
 * ----
 */

gulp.task('sass', function () {

  var processors = [
    autoprefixer(),
    mqpacker({
      sort: true
    }),
  ];

  return gulp.src('./theme/assets/sass/*.scss')
    .pipe(plumber(function (error) {
      gutil.log(error.message);
      this.emit('end');
    }))
    .pipe(sassGlob())
    .pipe(sass.sync().on('error', sass.logError))
    .pipe(postcss(processors))
    .pipe( through2.obj( function ( file, enc, cb ) {
      var date = new Date();
      file.stat.atime = date;
      file.stat.mtime = date;
      cb( null, file );
    }))
    .pipe(gulp.dest('./theme/assets/css'));
});


/**
 * CSS
 * ---
 * Creates min file while preserving the original
 */

gulp.task('css:minify', function () {
  
  var processors = [
    csswring()
  ];

  return gulp.src([
      './theme/assets/css/*.css',
      '!./theme/assets/css/*.min.css',
      '!./theme/assets/css/style.css'
    ])
    .pipe(clone())
    .pipe(rename(function (path) {
      path.basename += ".min";
      path.extname = ".css";
    }))
    .pipe(postcss(processors))
    .pipe( through2.obj( function ( file, enc, cb ) {
      var date = new Date();
      file.stat.atime = date;
      file.stat.mtime = date;
      cb( null, file );
    }))
    .pipe(gulp.dest('./theme/assets/css/'));
});



/**
 * JS
 * --
 */

gulp.task('js', function () {
  return gulp.src([
    './theme/assets/js/libs/*.js',
    './theme/assets/js/plugins/*.js',
    './theme/assets/js/*.js'
  ])
    .pipe(plumber())
    .pipe(concat('site.js'))
    .pipe( through2.obj( function ( file, enc, cb ) {
      var date = new Date();
      file.stat.atime = date;
      file.stat.mtime = date;
      cb( null, file );
    }))
    .pipe(gulp.dest('./theme/assets/js/build'));
});

gulp.task('js:minify', function () {
  return gulp.src([
    './theme/assets/js/build/*.js',
    '!./theme/assets/js/build/*.min.js',
  ])
    .pipe(clone())
    .pipe(uglify())
    .pipe(rename(function (path) {
      path.basename += ".min";
      path.extname = ".js";
    }))
    .pipe( through2.obj( function ( file, enc, cb ) {
      var date = new Date();
      file.stat.atime = date;
      file.stat.mtime = date;
      cb( null, file );
    }))
    .pipe(gulp.dest( './theme/assets/js/build/' ));
});



/**
 * Gulp
 * ----
 */

// Watch
gulp.task( 'watch', function task_watch() {

  // Watch js files
  watch([
    './theme/assets/js/**/*.js',
    '!./theme/assets/js/build/**/*.js',
  ]).on('change', gulp.series( 'js', 'js:minify' ));

  // Watch scss files
  watch([
    './theme/assets/sass/**/*.scss'
  ]).on('change', gulp.series( 'sass', 'css:minify' ));

});

// Default Task
gulp.task('default', gulp.series( 'watch' ));
